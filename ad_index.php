

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300,700,800' rel='stylesheet' type='text/css'>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">


<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
 

<link rel="stylesheet" type="text/css" href="css/admin-style.css">
<link rel="stylesheet" type="text/css" href="css/responcive.css">
<!--[if lt IE 9]>
	<script type="text/javascript" src="js/html5shiv.min.js"></script>
	<script type="text/javascript" src="js/respond.min.js"></script>
<![endif]-->

<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/cart.js"></script>

<title>ADMIN</title>

</head>

<body>
<div class="container no-r-l-padding pagewrap" >
<!--session
====================================-->
<?php
session_start();
if(ISSET($_SESSION['admin']))
{
	$user_name=$_SESSION["admin"];
}
else
{
	header('Location:login.php');
}
?>


<!--navpannel
====================================-->
<div class="col-lg-12 col-md-12  col-sm-12  col-xs-12 nav-pannel" >
	<div class=" logo-box">Desporto Admin</div>
    
    <div class="col-lg-1 notifi-box  ">
    <div class="avtar">
    <img src="images/avtar.jpg" class="img-circle" />
    <div class="logout"><a href="logout.php" onclick="<?php session_destroy();?>"  >Logout</a></div>
    
    </div>
    
    
    
    
    
    <div class=" message-icon">
    <div class="hot">10</div>
    
    <span class=" glyphicon glyphicon-envelope "></span>
    
    <div class="in-box  ">
    
    <div class="in-box-in">
    <h1>Messages</h1>
    	<li >        
         <a href="#">vcb cvcvbvccvbcboipipopu jyju ghujg fdhdf</a> 
        <p>After you get up and running, you can place Font Awesome icons just about anywhere with theAfter you get up and running...</p>
        </li>
        
        
      	<li >        
         <a href="#">vcb cvcvbvccvbcboipipopu jyju ghujg fdhdf</a> 
        <p>After you get up and running, you can place Font Awesome icons just about anywhere with theAfter you get up and running...</p>
        </li>
        
        
        <li >        
         <a href="#">vcb cvcvbvccvbcboipipopu jyju ghujg fdhdf</a> 
        <p>After you get up and running, you can place Font Awesome icons just about anywhere with theAfter you get up and running...</p>
        </li>
    
    
    
    <div class="approve"> <a href="#">Go to Approve</a></div>
    </div>
    </div>    
    </div>
    </div>
</div>

<!--navpannel-end
====================================-->


<!--admin-left
==============================-->
<div class=" col-lg-3 col-md-4 col-sm-4 col-xs-12 " >
<div class="   sidebar-offcanvas admin-left  " id="sidebar">
          <div class="ad-list-group">
            <a href="ad_index.php" class="list-group-item active "><img src="images/dashbod-hed.png" />Dash board</a>
            <a href="ad_club-register-requist.php" class="list-group-item "><img src="images/club-reqst.png" />Club registration requist </a>
            <a href="ad_add-coaches.php" class="list-group-item"><img src="images/add-cochs.png" />Add coaches</a>
            <a href="ad_host-event.php" class="list-group-item"><img src="images/launch.png" />Host event</a>
            <a href="ad_post-new-blog.php" class="list-group-item"><img src="images/new-post.png" />Post new blog</a>
			<a href="ad_score_update.php" class="list-group-item"><img src="images/new-post.png" />Update Score</a>
            
            <a class="list-group-item"></a>
            <a class="list-group-item"></a>
            <a class="list-group-item"></a>
            <a class="list-group-item"></a>
            <a class="list-group-item"></a><a class="list-group-item"></a>
            
            
             
          </div>
        </div>
</div>

<div class=" col-lg-3 col-md-4 col-sm-4 col-xs-12 " >
<div class="   sidebar-offcanvas admin-left  " id="sidebar">
          <div class="list-group">
          </div>
          </div>
          </div>
          
<!--admin-left-end
==============================-->


<!--admin-content
==============================-->

<div class="col-lg-6 col-md-5 col-sm-4 col-xs-12 " style="border-right:1px solid #ccc; min-height:200px; max-height:200px"><br/><br/><br/><b> Welcome Admin !!</b></div>

<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 "> </div>


<!--admin-content-end
==============================-->





</div>


 

<script type="text/javascript">
  WebFontConfig = {
    google: { families: [ 'Open+Sans:300italic,400,300,700,800:latin' ] }
  };
  (function() {
    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
      '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
  })(); </script>
  

</body>
</html>
