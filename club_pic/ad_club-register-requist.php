<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300,700,800' rel='stylesheet' type='text/css'>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">


<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
 

<link rel="stylesheet" type="text/css" href="css/admin-style.css">
<link rel="stylesheet" type="text/css" href="css/responcive.css">
<!--[if lt IE 9]>
	<script type="text/javascript" src="js/html5shiv.min.js"></script>
	<script type="text/javascript" src="js/respond.min.js"></script>
<![endif]-->

<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/cart.js"></script>

<title>ADMIN</title>

</head>



<body>

<?php



$server="localhost";
$user="root";
$pass="";
$db="deacademy";

$conn = mysqli_connect($server,$user,$pass,$db);

$club_name_array = array();
$reg_no_array = array();
$activity_array = array();
$name_array = array();
$email_array = array();
$dis_array = array();
$addr_array = array();
$state_array = array();
$img_array = array();

$ap_club_array = array();
$ap_reg_array = array();
$ap_act_array = array();




$query = mysqli_query($conn, "SELECT * FROM registration WHERE apflag = '0' ") or die("Error :" .mysqli_error($conn));
while($row = mysqli_fetch_array($query)){

  array_push($club_name_array, $row['club_name']);
  array_push($reg_no_array, $row['register_no']);
  array_push($activity_array, $row['activity']);
  array_push($addr_array, $row['address']);
  array_push($state_array, $row['state']);
  array_push($dis_array, $row['district']);
  array_push($email_array, $row['email']);
  array_push($name_array, $row['full_name']);
  array_push($img_array, $row['image']);


}

$result = mysqli_query($conn, "SELECT * FROM registration WHERE apflag = '1' ") or die("Error :" .mysqli_error($conn));
while($row = mysqli_fetch_array($result)){

  array_push($ap_club_array, $row['club_name']);
  array_push($ap_reg_array, $row['register_no']);
  array_push($ap_act_array, $row['activity']);

}


if(isset($_POST['approve'])){

  mysqli_query($conn, "UPDATE registration SET apflag = '1' WHERE apflag = '0'") or die("Error :" .mysqli_error($conn));
}

?>

<div class="container  no-r-l-padding pagewrap" >


<!--navpannel
====================================-->
<div class="col-lg-12 col-md-12  col-sm-12  col-xs-12  nav-pannel  " >
	<div class=" logo-box">Desporto Admin</div>
    
    <div class="col-lg-1 notifi-box  ">
    <div class="avtar">
    <img src="images/avtar.jpg" class="img-circle" />
    <div class="logout"><a href="#">Logout</a></div>
    
    </div>
    
    
    
    
    
    <div class=" message-icon">
    <div class="hot">10</div>
    
    <span class=" glyphicon glyphicon-envelope "></span>
    
    <div class="in-box  ">
    
    <div class="in-box-in">
    <h1>Messages</h1>
    	<li >        
         <a href="#">vcb cvcvbvccvbcboipipopu jyju ghujg fdhdf</a> 
        <p>After you get up and running, you can place Font Awesome icons just about anywhere with theAfter you get up and running...</p>
        </li>
        
        
      	<li >        
         <a href="#">vcb cvcvbvccvbcboipipopu jyju ghujg fdhdf</a> 
        <p>After you get up and running, you can place Font Awesome icons just about anywhere with theAfter you get up and running...</p>
        </li>
        
        
        <li >        
         <a href="#">vcb cvcvbvccvbcboipipopu jyju ghujg fdhdf</a> 
        <p>After you get up and running, you can place Font Awesome icons just about anywhere with theAfter you get up and running...</p>
        </li>
    
    
    
    <div class="approve"> <a href="#">Go to Approve</a></div>
    </div>
    </div>    
    </div>
    </div>
</div>

<!--navpannel-end
====================================-->

 
<!--admin-left
==============================-->
<div class=" col-lg-3 col-md-4 col-sm-4 col-xs-12 " >

<div class=" sidebar-offcanvas admin-left" id="sidebar">
          <div class="list-group">
            <a href="ad_index.html" class="list-group-item active "><img src="images/dashbod-hed.png" />Dash board</a>
            <a href="ad_club-register-requist.html" class="list-group-item focus"><img src="images/club-reqst.png" />Club registration requist<span class="focus-arrow"></span></a>
            <a href="ad_add-coaches.html" class="list-group-item"><img src="images/add-cochs.png" />Add coaches</a>
            <a href="ad_host-event.html" class="list-group-item"><img src="images/launch.png" />Host event</a>
            <a href="ad_post-new-blog.html" class="list-group-item"><img src="images/new-post.png" />Post new blog</a>
            <a href="ad_score_update.html" class="list-group-item"><img src="images/new-post.png" />Update Score</a>
             
          </div>
        </div>
</div>

<!--admin-left-end
==============================-->

<!--admin-content
==============================-->
<div class="col-lg-9 col-md-8 col-sm-8 col-xs-12  admi-cont-wrp"  >

<div class="reg-req-modl ">

<button class="btn btn-success req-launch-btn  " data-toggle="modal" data-target="#myModal">View Registration Form</button>
<div id="myModal" class="modal  fade " tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
    <div class="modal-dialog model-width">
      <div class="modal-content ">

        <div class="modal-header reg-req-modl-hd ">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color:#FFFFFF; opacity:9">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Club Registration Request Form</h4>
        </div>
        <div class="modal-body">
          
          
<div class="table-responsive">
            <table border="1" bordercolor="#CCCCCC" class="table table-striped ">
              <thead>
                <tr>
                  <th valign="top">Club Name</th>
                  <th valign="top">Reg No.</th>
                  <th valign="top">Club Activities</th>
                  <th valign="top">State</th>
                  <th valign="top">District</th>
                  <th valign="top">Address</th>
                  <th valign="top">email</th>
                  <th valign="top">First name</th>
                  <th width="10" valign="top">Photo</th>
                  <th colspan="2" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td colspan="2" align="center">Status</td>
                    </tr>
                    <tr>
                      <td height="1" colspan="2" align="center" bgcolor="#cccccc"> </td>
                    </tr>
                    <tr>
                      <td width="10" height="5" align="center"></td>
                      <td width="10" height="5" align="center"></td>
                    </tr>
                    <tr>
                      <td width="10" align="center">View</td>
                      <td width="10" align="center">Action</td>
                    </tr>
                  </table></th>
                </tr>
              </thead>
              <tbody>
              <?php

                $target_dir = 'images/club_pic/';
                for($i=0;$i<count($club_name_array);$i++){

                  $image = $target_dir.$img_array[$i];

                ?>
                <tr>
                  <td align="left" valign="middle"><?php echo $club_name_array[$i]; ?></td>
                  <td align="left" valign="middle"><?php echo $reg_no_array[$i]; ?></td>
                  <td align="left" valign="middle"><?php echo $activity_array[$i]; ?></td>
                  <td align="left" valign="middle"><?php echo $state_array[$i]; ?></td>
                  <td align="left" valign="middle"><?php echo $dis_array[$i]; ?></td>
                  <td align="left" valign="middle"><?php echo $addr_array[$i]; ?></td>
                  <td align="left" valign="middle"><?php echo $email_array[$i]; ?></td>
                  <td align="left" valign="middle"><?php echo $name_array[$i]; ?></td>
                  <td align="left" valign="middle">&nbsp;</td>
                  <td width="10"><?php echo '<img src="'.$image.'" width="73" height="73" />'?></td>
                  <td width="15" align="center" valign="middle"><button type="submit" class="btn btn-success">View</button></td>
                  <td width="15" align="center" valign="middle"><a href="#" class="btn btn-danger">Delete</a></td>
                </tr> 
                <?php } ?>               
              </tbody>
            </table>
</div>

                 
        </div>
        <div class="modal-footer">
        <form method = post>
          <button type="button" class="btn  cancl-btn" data-dismiss="modal">Cancel</button>
          <button type="submit" name="approve" class="btn  apprve-btn">Approve</button>
        </form>
        </div>

      </div>
    </div>
  </div>
</div>


<div class=" col-lg-12 approved-list" >

<h1>Approved Clubs</h1>
<div class="table-responsive res-tabe-2">

 <table border="1" bordercolor="#CCCCCC" class="table table-striped " width="100%">
              <thead>
                <tr>
                  <th valign="top">Club Name</th>
                  <th valign="top">Reg No.</th>
                  <th valign="top">Club Activities</th>
                  <th colspan="2" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td colspan="2" align="center">Status</td>
                    </tr>
                    <tr>
                      <td height="1" colspan="2" align="center" bgcolor="#cccccc"> </td>
                    </tr>
                    <tr>
                      <td width="10" height="5" align="center"></td>
                      <td width="10" height="5" align="center"></td>
                    </tr>
                    <tr>
                      <td width="10" align="center">View</td>
                      <td width="10" align="center">Action</td>
                    </tr>
                  </table></th>
                </tr>
              </thead>
              <tbody>
              <?php for($i=0;$i<count($ap_club_array);$i++){ 

                  
                    ?>
                <tr>
                  <td align="left" valign="middle"><?php echo $ap_club_array[$i]; ?></td>
                  <td align="left" valign="middle"><?php echo $ap_reg_array[$i]; ?></td>
                  <td align="left" valign="middle"><?php echo $ap_act_array[$i]; ?></td>
                  <td width="15" align="center" valign="middle"><a href="#" class="btn btn-success">View</a></td>
                  <td width="15" align="center" valign="middle"><a href="#" class="btn btn-danger">Remove</a></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
</div>
</div>
</div>




 


<!--admin-content-end
==============================-->
 







</div>


 

<script type="text/javascript">
  WebFontConfig = {
    google: { families: [ 'Open+Sans:300italic,400,300,700,800:latin' ] }
  };
  (function() {
    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
      '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
  })(); </script>
  

</body>
</html>
